<?php

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile.
 */
function reviewboard_profile_details() {
  return array(
    'name' => 'Reviewboard',
    'description' => 'This profile serves as the distro for the Reviewboard Drupal distribution.'
  );
}

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *  An array of modules to be enabled.
 */
function reviewboard_profile_modules() {
  return array(
    /* optional core */
    'menu', 'dblog', 'help', 'search', 'update', 'path', 'trigger',
    /* Other Contrib */
    'content', 
    'autocomplete_widgets', 
    'content_permissions', 
    'filefield', 
    'fieldgroup', 
    'text', 
    'optionwidgets', 
    'cck_required_single_select',
    'nodereference', 
    'nodereference_url', 
    'userreference',
    'date_api', 
    'date_timezone', 
    'date', 
    'date_popup', 
    'jquery_update', 
    'jquery_ui', 
    'modalframe', 
    'automodal', 
    'ctools', 
    'boxes', 
    'context', 
    'context_ui', 
    'features', 
    'strongarm', 
    'auto_nodetitle', 
    'multicolumncheckboxesradios', 
    'token', 
    'views', 
    'views_ui', 
    'better_exposed_filters', 
    'views_filters_reset', 
    'views_calc', 
    'views_slideshow', 
    'views_slideshow_singleframe', 
    'workflow', 
    'workflow_access', 
    'workflow_userreference_access', 
    'workflow_extensions', 
    'vertical_tabs',
    'jammer',
  );
}

/**
* Implementation of hook_profile_tasks().
*/
function reviewboard_profile_tasks(&$task, $url) {

  // Set site frontpage variable.
  variable_set('site_frontpage', 'schedule_front');

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);
  variable_set('theme_default', 'garland');
  variable_set('admin_theme', 'garland');

  // Configure the timezone settings.
  // variable_set('date_default_timezone_name', 'America/New_York');
  // variable_set('date_default_timezone', -18000);
  // variable_set('configurable_timezones', 0);

  /* Enable our custom features */
  $features = array(
    'reviewboard_base',
    'reviewboard_investment',
    'reviewboard_review',
    'reviewboard_review_workflow',
    'reviewboard_scoresheet',
    'reviewboard_scoresheet_workflow',
  );
  foreach ($features as $feature) {
    features_install_modules(array($feature));
    module_rebuild_cache();
    features_rebuild();
    features_revert();
  }

  drupal_install_modules(array('reviewboard_notifications'));
  features_rebuild();
  features_revert();

  // Update the menu router information.
  menu_rebuild();
  // Update content permissions.
  node_access_rebuild();
  // Clear all cache.
  cache_clear_all();
}
/**
 * Implementation of hook_form_alter().
 *
 * Allows the profile to alter the site-configuration form. This is
 * called through custom invocation, so $form_state is not populated.
 */
function reviewboard_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {
    // Set default for site name field.
    $form['site_information']['site_name']['#default_value'] = 'Review Board';
  }
}

